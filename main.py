import requests
from bs4 import BeautifulSoup
import csv
import time

# URL 
#base_url = "https://"

# Liste, zum speichern, für die fehlerhaften URLs
fehlerhafte_urls = []

# Status-Code Feedback
feedback = {
    200: "Erfolgreich",
    404: "Nicht gefunden",
    500: "Server-Fehler",
    # Du kannst hier mehr Statuscodes hinzufügen, wenn nötig.
}

# 404- Prüfung
def pruefe_url(url):
    # Wenn die URL nicht mit http oder https beginnt, dann kombiniere sie mit der Basis-URL
    if not url.startswith(('http://', 'https://')):
        url =  url.strip()  # strip() entfernt überflüssige Whitespaces und Zeilenumbrüche
    
    try:
        response = requests.get(url)
        status_code = response.status_code
        
        # Feedback für jeden Status-Code
        print(f"URL: {url} - Status-Code: {status_code} - {feedback.get(status_code, 'Unbekannter Status-Code')}")
        
        if status_code == 404:
            fehlerhafte_urls.append(url)

    except requests.exceptions.RequestException as e:  # Dies fängt alle Arten von Requests-Fehlern ab
        print(f"Fehler beim Abrufen der URL {url}: {e}")
        fehlerhafte_urls.append(url)
    
    time.sleep(2)  # 2 Sekunden Pause nach jeder Abfrag

# Einlesen der URLs aus der txt-Datei
with open("url.txt", "r") as f:
    seiten = f.readlines()

# Durchlauf 404- Prüfung
for seite in seiten:
    url =  seite.strip()  # strip() entfernt überflüssige Whitespaces und Zeilenumbrüche
    pruefe_url(url)

# Ausgabe Anzahl fehlerhafter URLs & Auflistung der fehlerhaften URLs
print(f"Gesamtanzahl der fehlerhaften URLs: {len(fehlerhafte_urls)}")
print("Fehlerhafte URLs:")
for fehlerhafte_url in fehlerhafte_urls:
    print(fehlerhafte_url)

# Speichern der fehlerhaften URLs in einer CSV-Datei
with open("fehlerhafte_urls.csv", "w", newline='') as file:
    writer = csv.writer(file)
    writer.writerow(["Fehlerhafte URLs"])
    for fehlerhafte_url in fehlerhafte_urls:
        writer.writerow([fehlerhafte_url])
